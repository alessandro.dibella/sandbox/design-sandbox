PARENT_PROJECT_DIR:=$(dir $(abspath $(lastword $(MAKEFILE_LIST))))
OUTPUT_DIR?=$(PARENT_PROJECT_DIR)out
LIQUIBASE_DIR?=$(PARENT_PROJECT_DIR)src/main/liquibase

include $(PARENT_PROJECT_DIR)build-scripts/docker/Makefile

DB_IS_NOT_INITIALIZED=$(shell docker volume inspect design-sandbox_postgres-data > /dev/null 2>&1; echo $$?)
DB_NAME:=sandbox
DB_USER_ROLE:=$(DB_NAME)_user_role
DB_USER_USERNAME:=$(DB_NAME)_user
DB_USER_PASSWORD:=$(DB_NAME)
DB_ADMIN_ROLE:=$(DB_NAME)_admin_role
DB_ADMIN_USERNAME:=$(DB_NAME)_admin
DB_ADMIN_PASSWORD:=$(DB_NAME)

PSQL := docker run --network sandbox --rm -it -v $(PARENT_PROJECT_DIR):/project -e PGPASSWORD='postgres' $(DB_CLIENT_OCI_IMAGE_NAME) psql -h db -U postgres -v ON_ERROR_STOP=1 -v database_name=$(DB_NAME) -v database_user_role=$(DB_USER_ROLE) -v database_user_username=$(DB_USER_USERNAME) -v database_user_password=$(DB_USER_PASSWORD) -v database_admin_role=$(DB_ADMIN_ROLE) -v database_admin_username=$(DB_ADMIN_USERNAME) -v database_admin_password=$(DB_ADMIN_PASSWORD)
LIQUIBASE := docker run --network sandbox --rm -it -v $(PARENT_PROJECT_DIR):/project $(DB_CLIENT_OCI_IMAGE_NAME) liquibase --verbose --username postgres --password postgres --url jdbc:postgresql://db:5432/sandbox --driver org.postgresql.Driver
IPYNB_EXECUTOR := docker run --network sandbox --rm -it -v $(PARENT_PROJECT_DIR):/project $(DATA_GENERATOR_OCI_IMAGE_NAME) /opt/conda/bin/conda run -n data-generator jupyter nbconvert --to notebook --execute --allow-errors --ExecutePreprocessor.timeout=18000 --stdout


# # .DEFAULT_GOAL := dist



$(OUTPUT_DIR)/liquibase_report.txt.gz: $(DB_CLIENT_IMAGE_DEPENDENCY) $(shell find $(LIQUIBASE_DIR) -type f) 
	mkdir -p $(OUTPUT_DIR)
	time $(LIQUIBASE) --changeLogFile=/project/src/main/liquibase/changeLog.xml update -Ddatabase.admin.role=$(DB_ADMIN_ROLE) -Ddatabase.user.role=$(DB_USER_ROLE) | tee /dev/tty | gzip --stdout > $(OUTPUT_DIR)/liquibase_report.txt.gz


start-db : $(DB_CLIENT_IMAGE_DEPENDENCY)
	docker-compose --project-name design-sandbox --file $(PARENT_PROJECT_DIR)/docker-compose.yml up --detach --remove-orphans
ifneq ($(DB_IS_NOT_INITIALIZED),0)
	for i in 1 2 3 4 5 6 7 8 9 10; do  \
		echo "Attempting to initialize the database in 10 seconds ($$i/10)";  \
		sleep 10;  \
		$(PSQL) -d postgres -f /project/src/main/sql/setup.sql && break;  \
	done
endif		

stop-db:
	docker-compose --project-name design-sandbox --file $(PARENT_PROJECT_DIR)/docker-compose.yml down 

migrate-db: $(OUTPUT_DIR)/liquibase_report.txt.gz	
	
clean-db: 
	docker-compose --project-name design-sandbox --file $(PARENT_PROJECT_DIR)/docker-compose.yml rm --stop --force -v 
	docker volume rm design-sandbox_postgres-data || true
	docker network rm sandbox || true
	rm -rf $(OUTPUT_DIR)/liquibase_report.txt.gz

clean: clean-db clean-oci-images
	@rm -rf $(OUTPUT_DIR)

generate-data: $(DATA_GENERATOR_IMAGE_DEPENDENCY)
	@echo "Generating data...it will take a while..."
	@time $(IPYNB_EXECUTOR) /project/src/test/python/data_generator.ipynb

# run-uuid-test: $(OUTPUT_DIR)/aldib-taurus.json.gz $(OUTPUT_DIR)/liquibase_report.txt.gz
# 	rm -rf $(OUTPUT_DIR)/uuid_load_test*.csv $(OUTPUT_DIR)/jmeter.log; \
# 	time $(TAURUS) /project/src/test/taurus/UUID_test_plan.jmx ; \