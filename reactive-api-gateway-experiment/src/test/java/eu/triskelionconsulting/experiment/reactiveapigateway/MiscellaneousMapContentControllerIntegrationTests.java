package eu.triskelionconsulting.experiment.reactiveapigateway;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Arrays;
import java.util.stream.Collectors;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MiscellaneousMapContentControllerIntegrationTests {    

    @Autowired
    private WebTestClient rest;

    private static final String QUERYABLE_PROPERTIES = Arrays.stream(AbstractMiscellaneousContentRepository.QUERYABLE_PROPERTIES).collect(Collectors.joining(","));

    @Test
    void testFindById() {
        rest
            .get()
            .uri("/map/misc/{gid}?properties={properties}","cefe75d5-fdea-5d9d-b5a2-d1f3a1ffd6c4",QUERYABLE_PROPERTIES)
            .exchange()
            .expectStatus().is2xxSuccessful()
            .expectBody().consumeWith(System.out::println);
        System.out.println();
    }

    @Test
    void testAll() {
        rest
            .get()
            .uri("/map/misc/?properties={properties}",QUERYABLE_PROPERTIES)
            .exchange()
            .expectStatus().is2xxSuccessful()
            .expectBody().consumeWith(System.out::println);
        System.out.println();
    } 

}
