package eu.triskelionconsulting.experiment.reactiveapigateway.orm;

import eu.triskelionconsulting.experiment.reactiveapigateway.AbstractMiscellaneousContentRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.stereotype.Component;

@Component("miscellaneousEntityContentRepository")
public class MiscellaneousOrmContentRepository extends AbstractMiscellaneousContentRepository<MiscellaneousContentEntity> implements InitializingBean {

    @Autowired
    private R2dbcEntityTemplate entityTemplate;


    public MiscellaneousOrmContentRepository() {
        super(MiscellaneousContentEntity.class);
    }

    @Override
    protected R2dbcEntityTemplate getTemplate() {
        return entityTemplate;
    }


}