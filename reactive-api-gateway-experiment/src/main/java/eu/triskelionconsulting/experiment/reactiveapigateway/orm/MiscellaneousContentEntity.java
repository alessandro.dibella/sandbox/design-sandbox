package eu.triskelionconsulting.experiment.reactiveapigateway.orm;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Map;
import java.util.UUID;

@Data

@Table("miscellaneous_content")
public class MiscellaneousContentEntity {

    @Id
    @Column("gid")
    private UUID gid;
    
    @Column("creation_ts")
    private OffsetDateTime creationTs;
    @Column("last_update_ts")
    private OffsetDateTime lastUpdateTs;
    @Column("deletion_ts")
    private OffsetDateTime deletionTs;
    @Column("id")
    private long id;
    @Column("refs")
    private Map<String,String> refs;
    @Column("string")
    private String string;
    @Column("integer_number")
    private Long integerNumber;
    @Column("decimal_number")
    private Float decimalNumber;
    @Column("json_obj")
    private Map<String,?> jsonObj;
    @Column("monetary_value")
    private BigDecimal monetaryValue;
    @Column("string_array")
    private String[] stringArray;
    @Column("decimal_array")
    private BigDecimal[] decimalArray;
    @Column("ts_array")
    private OffsetDateTime[] tsArray;

}
