package eu.triskelionconsulting.experiment.reactiveapigateway.map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.triskelionconsulting.experiment.reactiveapigateway.NullObject;
import io.r2dbc.postgresql.type.PostgresqlObjectId;
import io.r2dbc.spi.ColumnMetadata;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import io.timeandspace.smoothie.SmoothieMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.serializer.support.SerializationFailedException;
import org.springframework.data.r2dbc.convert.MappingR2dbcConverter;
import org.springframework.data.relational.core.mapping.RelationalMappingContext;

import java.io.IOException;
import java.util.Map;


public class MapR2dbcConverter extends MappingR2dbcConverter{

    private Logger log = LoggerFactory.getLogger(MapR2dbcConverter.class);

    private final ObjectMapper mapper;
    
    public MapR2dbcConverter(ObjectMapper mapper){
        super(new RelationalMappingContext());
        this.mapper = mapper;
    }

    @Override
    public <R> R read(Class<R> type, Row source, RowMetadata metadata) {        
        if (!type.isAssignableFrom(Map.class)){
            throw new IllegalArgumentException("This converter can only create instances of ["+Map.class.getName()+"]");
        }

        Map<String, Object> root = SmoothieMap.<String, Object>newBuilder().build();
        metadata.getColumnMetadatas().forEach(cm->{        
            String column = cm.getName();    
            Object nativeType = cm.getNativeTypeMetadata();
            Object value;
            if (nativeType.equals(PostgresqlObjectId.JSONB.getObjectId())){
                try {
                    value = mapper.readValue((byte[]) source.get(column, byte[].class), Map.class);
                } catch (IOException e) {
                    throw new SerializationFailedException("Unable to parse content of column ["+column+"] into json",e);
                }
            }else{
                value = source.get(column);
            }           
            log.debug("Found column ["+column+"] of native type ["+nativeType+"] with value ["+value+"] of type  ["+((value==null) ? "" : value.getClass().getName())+"]");
            if (value!=null){
                root.put(column, value);
            }else{
                root.put(column, NullObject.NULL);
            }
        });
        
        try {
            log.debug("Returning result \n"+mapper.writeValueAsString(root)+"");
        } catch (JsonProcessingException e) {
            log.warn("Cannot log",e);
        }
        return (R) root;
    }
}
