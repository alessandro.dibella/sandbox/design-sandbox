package eu.triskelionconsulting.experiment.reactiveapigateway.orm;

import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.util.MimeType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/orm/misc")
public class MiscellaneousOrmContentController {

    @Autowired
    private MiscellaneousOrmContentRepository repository;

    /** This is a workaround described in https://github.com/spring-projects/spring-framework/issues/26406.
     *  It's not a great solutions because  {@link Flux#collectList()}} will consume all records and build an in memory list from them
     *  Depending on the size of the list, it might be a very memory-heavy approach.
     *  Nevertheless, {@link org.springframework.http.codec.json.AbstractJackson2Encoder#encode(Publisher, DataBufferFactory, ResolvableType, MimeType, Map)}
     *  does the same unless streaming is enabled.
     *
     *  TODO Investigate use of {@link MediaType#APPLICATION_NDJSON_VALUE} as streaming content type
     * 
     *  See https://stackoverflow.com/a/54687521 for more details
     */
    protected Publisher<MappingJacksonValue> filter(Flux<?> data, String[] properties){
        FilterProvider filters = new SimpleFilterProvider()
            .addFilter("queriedPropertiesFilter",SimpleBeanPropertyFilter.filterOutAllExcept(properties));

        return data
            .collectList()
            .map(MappingJacksonValue::new)
            .doOnNext(value -> value.setFilters(filters));
    }

    @GetMapping("/{id}")
    private Publisher<?> getById(@PathVariable UUID id, @RequestParam(name = "properties", defaultValue = "gid,id,refs") String[] properties, ServerHttpResponse response) {
        SimpleFilterProvider filter = new SimpleFilterProvider().addFilter("queriedPropertiesFilter",SimpleBeanPropertyFilter.filterOutAllExcept(properties));
        return repository.findById(id, properties).map(MappingJacksonValue::new).doOnNext(v->v.setFilters(filter));
    }


    @GetMapping(value="/")
//    @GetMapping(value="/", produces = MediaType.APPLICATION_NDJSON_VALUE)
    private Publisher<?> getAll(@RequestParam(name = "properties", defaultValue = "gid,id,refs") String[] properties, @RequestParam(name= "cursor", defaultValue = "0") long cursor, @RequestParam(name = "offset", defaultValue = "10") int offset, ServerHttpResponse response) {
        if (offset>0){
            return filter(repository.findAllAfter(properties, cursor, offset),properties);
        }else if (offset<0){
            return filter(repository.findAllBefore(properties, cursor, offset*-1),properties);
        }else{
            response.setStatusCode(HttpStatus.BAD_REQUEST);
            return Flux.just("Parameter [offset] cannot be null");
        }
    }

}
