package eu.triskelionconsulting.experiment.reactiveapigateway;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;


public final class NullObject {

	public final static NullObject NULL = new NullObject();

	private NullObject(){}


	public static class NullObjectJsonSerializer extends JsonSerializer<NullObject>{
		@Override
		public void serialize(NullObject nullObject, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
			jsonGenerator.writeNull();
		}
	}

	@Override
	public String toString() {
		return "null";
	}


}
