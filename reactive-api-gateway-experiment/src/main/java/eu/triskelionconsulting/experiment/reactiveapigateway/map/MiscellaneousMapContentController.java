package eu.triskelionconsulting.experiment.reactiveapigateway.map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController
@RequestMapping("/map/misc")
public class MiscellaneousMapContentController {

    @Autowired
    private MiscellaneousMapContentRepository repository;

    @GetMapping("/{id}")
    private Mono<?> getById(@PathVariable UUID id, @RequestParam(name = "properties", defaultValue = "gid,id,refs") String[] properties, ServerHttpResponse response) {
        return repository.findById(id, properties);
    }

    @GetMapping("/")
    private Flux<?> getAll(@RequestParam(name = "properties", defaultValue = "gid,id,refs") String[] properties, @RequestParam(name= "cursor", defaultValue = "0") long cursor, @RequestParam(name = "offset", defaultValue = "10") int offset, ServerHttpResponse response) {
        if (offset>0){
            return repository.findAllAfter(properties, cursor, offset);
        }else if (offset<0){
            return repository.findAllBefore(properties, cursor, offset*-1);
        }else{
            response.setStatusCode(HttpStatus.BAD_REQUEST);
            return Flux.just("Parameter [offset] cannot be null");                        
        }
    }
    
}
