package eu.triskelionconsulting.experiment.reactiveapigateway.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.r2dbc.postgresql.type.PostgresqlObjectId;
import io.r2dbc.spi.ColumnMetadata;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.serializer.support.SerializationFailedException;
import org.springframework.data.r2dbc.convert.MappingR2dbcConverter;
import org.springframework.data.relational.core.mapping.RelationalMappingContext;

import java.io.IOException;


public class JsonNodeR2dbcConverter extends MappingR2dbcConverter{

    private Logger log = LoggerFactory.getLogger(JsonNodeR2dbcConverter.class);

    
    private final ObjectMapper mapper;

    public JsonNodeR2dbcConverter(ObjectMapper mapper){
        super(new RelationalMappingContext());
        this.mapper = mapper;
    }

    @Override
    public <R> R read(Class<R> type, Row source, RowMetadata metadata) {        
        if (!type.isAssignableFrom(JsonNode.class)){
            throw new IllegalArgumentException("This converter can only create instances of ["+JsonNode.class.getName()+"]");
        }
        ObjectNode root = (ObjectNode) mapper.createObjectNode();
        metadata.getColumnMetadatas().forEach(cm->{        
            String column = cm.getName();            
            Object nativeType = cm.getNativeTypeMetadata();
            JsonNode value;
            if (nativeType.equals(PostgresqlObjectId.JSONB.getObjectId())){
                try {
                    value = mapper.readTree((byte[]) source.get(column, byte[].class));
                } catch (IOException e) {
                    throw new SerializationFailedException("Unable to parse content of column ["+column+"] into json",e);
                }
            }else{
                value = mapper.getNodeFactory().pojoNode(source.get(column));
            }           
            log.debug("Found column ["+column+"] of native type ["+nativeType+"] with value ["+value.asText()+"] of type  ["+((value==null) ? "" : value.getClass().getName())+"]");
            // root.set(column, onValue(value));
            root.set(column, value);
        });
        
        try {
            log.debug("Returning result \n"+mapper.writeValueAsString(root)+"");
        } catch (JsonProcessingException e) {
            log.warn("Cannot log",e);
        }
        return (R) root;
    }
}
