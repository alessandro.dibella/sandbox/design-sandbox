package eu.triskelionconsulting.experiment.reactiveapigateway.map;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.triskelionconsulting.experiment.reactiveapigateway.AbstractMiscellaneousContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.r2dbc.dialect.DialectResolver;
import org.springframework.data.r2dbc.dialect.R2dbcDialect;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component("miscellaneousMapContentRepository")
public class MiscellaneousMapContentRepository extends AbstractMiscellaneousContentRepository<Map> {

    private R2dbcEntityTemplate entityTemplate;

    public MiscellaneousMapContentRepository() {
        super(Map.class);
    }

    @Autowired
    public void setTemplate(R2dbcEntityTemplate template, ObjectMapper mapper){
        DatabaseClient client = template.getDatabaseClient();
        R2dbcDialect dialect = DialectResolver.getDialect(client.getConnectionFactory());
        this.entityTemplate = new R2dbcEntityTemplate(client,dialect, new MapR2dbcConverter(mapper));
    }

    @Override
    protected R2dbcEntityTemplate getTemplate() {
        return entityTemplate;
    }
}
