package eu.triskelionconsulting.experiment.reactiveapigateway;

import eu.triskelionconsulting.experiment.reactiveapigateway.orm.MiscellaneousContentEntity;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mapping.context.MappingContext;
import org.springframework.data.r2dbc.convert.R2dbcConverter;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.mapping.RelationalPersistentEntity;
import org.springframework.data.relational.core.mapping.RelationalPersistentProperty;
import org.springframework.data.relational.core.sql.SqlIdentifier;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.data.domain.Sort.Order.desc;
import static org.springframework.data.domain.Sort.by;
import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

public abstract class AbstractMiscellaneousContentRepository<T> implements InitializingBean {

    public static final String[] QUERYABLE_PROPERTIES ={
            "gid",
            "creationTs",
            "lastUpdateTs",
            "deletionTs",
            "id",
//            "refs",
            "string",
            "integerNumber",
            "decimalNumber",
//            "jsonObj",
            "monetaryValue",
            "stringArray",
            "decimalArray",
//            "tsArray"
    };

    @Autowired
    private R2dbcConverter converter;

    private RelationalPersistentEntity<RelationalPersistentProperty> persistentEntity;

    public static final Object NULL_VALUE = new Object();

    private final Class<T> entityType;

    public AbstractMiscellaneousContentRepository(Class<T> entityType) {
        this.entityType = entityType;
    }

    protected abstract R2dbcEntityTemplate getTemplate();

    public Class<T> getEntityType() {
        return entityType;
    }

    protected Set<String> getProjection(String[] projection){
        return Arrays.stream(projection)
            .map(persistentEntity::getRequiredPersistentProperty)
            .map(RelationalPersistentProperty::getColumnName)
            .map(SqlIdentifier::getReference)
            .collect(Collectors.toSet());
    }


    public Mono<T> findById(UUID gid, String[] projection){
        return getTemplate().select(entityType)
                .from("mockdata.miscellaneous_content")
                .matching(
                        query(
                                where("gid").is(gid)
                        )
                                .columns(getProjection(projection))
                ).one();
    }

    /**
     * https://relay.dev/graphql/connections.htm#sec-Backward-pagination-arguments
     */
    public Flux<T> findAllBefore(String[] projection, long cursor, int before){
        return getTemplate().select(entityType)
                .from("mockdata.miscellaneous_content")
                .matching(
                        query(
                                where("id").lessThan(cursor)
                        )
                                .columns(getProjection(projection))
                                .limit(before)
                                .sort(by(desc("id")))
                ).all();
    }


    /**
     * https://relay.dev/graphql/connections.htm#sec-Forward-pagination-arguments
     */
    public Flux<T> findAllAfter(String[] projection, long cursor, int after){
        return getTemplate().select(entityType)
                .from("mockdata.miscellaneous_content")
                .matching(
                        query(
                                where("id").greaterThan(cursor)
                        )
                                .columns(getProjection(projection))
                                .limit(after)
                ).all();
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        MappingContext<? extends RelationalPersistentEntity<?>, ? extends RelationalPersistentProperty> context = converter.getMappingContext();
        this.persistentEntity = (RelationalPersistentEntity<RelationalPersistentProperty>) context.getRequiredPersistentEntity(MiscellaneousContentEntity.class);
    }

}
