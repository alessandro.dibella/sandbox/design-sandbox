package eu.triskelionconsulting.experiment.reactiveapigateway;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ReactiveApiGatewayExperimentApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactiveApiGatewayExperimentApplication.class, args);
	}

	@Bean
	public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer() {

		return builder -> builder
			.modulesToInstall(new JavaTimeModule())
			.serializerByType(NullObject.class,new NullObject.NullObjectJsonSerializer())

			.serializationInclusion(JsonInclude.Include.NON_NULL)				
//			.featuresToDisable(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS)
			.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)

			.indentOutput(true)
			.visibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE)
			.visibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

	}
}
