DO $$
DECLARE r record;
BEGIN
    FOR counter IN 1..250 LOOP
        INSERT INTO mockdata.miscellaneous_content (gid, id, refs, string, integer_number, decimal_number, json_obj, monetary_value, string_array, decimal_array, ts_array) 
        SELECT        
                uuid_generate_v5 ( 'a9234504-5747-4821-92f9-9c1fa55fcf4b'::uuid, nextval('mockdata.miscellaneous_content_seq')::text) as gid,
                currval('mockdata.miscellaneous_content_seq') as id,
                hstore('SSOR','#'||currval('mockdata.miscellaneous_content_seq'))::jsonb as refs,
                MD5(random()::text) as string,
                floor(random() * 30000 + 1)::int as integer_number,
                (random() * 10000 + 1)::double precision as decimal_number,
                ('{"a":'|| floor(random() * 30000 + 1) || ', "b":"'|| MD5(random()::text)  ||'"}')::jsonb as json_obj,
                (random() * 1000 + 1)::numeric(10,6) as monetary_value,
                ARRAY[MD5(random()::text),MD5(random()::text)]::text[] as string_array,
                ARRAY[(random() * 10000 + 1),(random() * 10000 + 1)]::double precision[] as decimal_array,
                ARRAY[current_timestamp - (floor(random() * 100 + 1)::int || ' day')::interval,current_timestamp - (floor(random() * 100 + 1)::int || ' day')::interval]::timestamp with time zone[] as ts_array
        FROM generate_series(1, 10000);

        COMMIT;
    END LOOP;
END $$;