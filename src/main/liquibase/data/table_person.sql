--liquibase formatted sql

--changeset alessandro.dibella:d9e779e8-a462-46f3-9426-5ff853e3badb dbms:postgresql
CREATE TABLE IF NOT EXISTS data.person
(
  salutation text,
  first_name text,
  middle_name text,
  last_name text,
  gender character(1),
  locale text NOT NULL DEFAULT 'en_GB',

  digest text GENERATED ALWAYS AS ( to_digest(
        to_text(refs),
        to_text(salutation),
        to_text(first_name),
        to_text(middle_name),
        to_text(last_name)
  ) ) STORED,
  
  PRIMARY KEY (gid)

) INHERITS (common.identifiable, common.auditable, common.soft_deletable, common.referenceable, common.versioned);

CREATE SEQUENCE data.person_id_seq START 1 NO CYCLE OWNED BY data.person.id;

ALTER TABLE data.person ALTER COLUMN id SET DEFAULT nextval('data.person_id_seq');

CREATE UNIQUE INDEX person_id_idx ON data.person USING BTREE (id);
CREATE INDEX person_refs_idx ON data.person USING GIN (refs);

ALTER TABLE data.person OWNER TO ${database.admin.role};
GRANT SELECT, UPDATE, INSERT ON data.person TO ${database.user.role};

ALTER SEQUENCE data.person_id_seq OWNER TO ${database.admin.role};
GRANT USAGE ON data.person_id_seq TO ${database.user.role};


