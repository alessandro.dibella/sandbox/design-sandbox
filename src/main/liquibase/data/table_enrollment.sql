--liquibase formatted sql

--changeset alessandro.dibella:495e26d2-d947-4569-b8c9-bee209ed81cb dbms:postgresql
CREATE TABLE IF NOT EXISTS data.enrollment
(
  student_id uuid NOT NULL,
  enrollment_ts timestamp with time zone NOT NULL DEFAULT current_timestamp,
  course_id uuid NOT NULL,
  teacher_id uuid NOT NULL,

  digest text GENERATED ALWAYS AS ( to_digest(
        to_text(student_id),        
        to_text(enrollment_ts),
        to_text(course_id),
        to_text(teacher_id)
  ) ) STORED,

  PRIMARY KEY (gid),
  CONSTRAINT fk_student_id FOREIGN KEY(student_id) REFERENCES data.student(gid) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT fk_teacher_id FOREIGN KEY(teacher_id) REFERENCES data.teacher(gid) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED

) INHERITS (common.identifiable, common.auditable, common.soft_deletable, common.versioned, common.referenceable);

CREATE UNIQUE INDEX enrollment_id_idx ON data.enrollment USING BTREE (id);
CREATE INDEX enrollment_student_id_idx ON data.enrollment USING HASH (student_id);
CREATE INDEX enrollment_teacher_id_idx ON data.enrollment USING HASH (teacher_id);

CREATE SEQUENCE data.enrollment_id_seq START 1 NO CYCLE OWNED BY data.enrollment.id;

ALTER TABLE data.enrollment ALTER COLUMN id SET DEFAULT nextval('data.enrollment_id_seq');

ALTER TABLE data.enrollment OWNER TO ${database.admin.role};
GRANT SELECT, UPDATE, INSERT ON data.enrollment TO ${database.user.role};

ALTER SEQUENCE data.enrollment_id_seq OWNER TO ${database.admin.role};
GRANT USAGE ON data.enrollment_id_seq TO ${database.user.role};


