--liquibase formatted sql

--changeset alessandro.dibella:4f1b8112-f6f0-4899-bcc6-13a1f4aa6856 dbms:postgresql
CREATE TABLE IF NOT EXISTS data.teacher
(
  person_id uuid NOT NULL,
  institution_id uuid NOT NULL,

  digest text GENERATED ALWAYS AS ( to_digest(
        to_text(refs),
        to_text(person_id),        
        to_text(institution_id)
  ) ) STORED,

  PRIMARY KEY (gid),
  CONSTRAINT fk_person_id FOREIGN KEY(person_id) REFERENCES data.person(gid) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT fk_institution_id FOREIGN KEY(institution_id) REFERENCES data.institution(gid) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED

) INHERITS (common.identifiable, common.auditable, common.soft_deletable, common.referenceable, common.versioned);

CREATE UNIQUE INDEX teacher_id_idx ON data.teacher USING BTREE (id);
CREATE INDEX teacher_refs_idx ON data.teacher USING GIN (refs);
CREATE INDEX teacher_person_id_idx ON data.teacher USING HASH (person_id);
CREATE INDEX teacher_institution_id_idx ON data.teacher USING HASH (institution_id);

CREATE SEQUENCE data.teacher_id_seq START 1 NO CYCLE OWNED BY data.teacher.id;

ALTER TABLE data.teacher ALTER COLUMN id SET DEFAULT nextval('data.teacher_id_seq');

ALTER TABLE data.teacher OWNER TO ${database.admin.role};
GRANT SELECT, UPDATE, INSERT ON data.teacher TO ${database.user.role};

ALTER SEQUENCE data.teacher_id_seq OWNER TO ${database.admin.role};
GRANT USAGE ON data.teacher_id_seq TO ${database.user.role};


