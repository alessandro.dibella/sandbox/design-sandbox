--liquibase formatted sql

--changeset alessandro.dibella:f2b0f8ed-edc2-44a7-8281-09409ebbb5fe dbms:postgresql
CREATE SCHEMA  IF NOT EXISTS data AUTHORIZATION ${database.admin.role};


--changeset alessandro.dibella:1634f814-e329-4052-9afb-1dec84aed551 dbms:postgresql
GRANT USAGE ON SCHEMA data TO ${database.user.role};