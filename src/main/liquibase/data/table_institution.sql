--liquibase formatted sql

--changeset alessandro.dibella:794b1f39-00d4-4546-8141-54a8bf096870 dbms:postgresql
CREATE TABLE IF NOT EXISTS data.institution
(
  name text NOT NULL,
  country character(2) NOT NULL,

  digest text GENERATED ALWAYS AS ( to_digest(
        to_text(refs),
        to_text(name),
        to_text(country)
  ) ) STORED,
  
  PRIMARY KEY (gid)

) INHERITS (common.identifiable, common.auditable, common.soft_deletable, common.referenceable, common.versioned);

CREATE SEQUENCE data.institution_id_seq START 1 NO CYCLE OWNED BY data.institution.id;

ALTER TABLE data.institution ALTER COLUMN id SET DEFAULT nextval('data.institution_id_seq');

CREATE UNIQUE INDEX institution_id_idx ON data.institution USING BTREE (id);
CREATE INDEX institution_refs_idx ON data.institution USING GIN (refs);

ALTER TABLE data.institution OWNER TO ${database.admin.role};
GRANT SELECT, UPDATE, INSERT ON data.institution TO ${database.user.role};

ALTER SEQUENCE data.institution_id_seq OWNER TO ${database.admin.role};
GRANT USAGE ON data.institution_id_seq TO ${database.user.role};


