--liquibase formatted sql

--changeset alessandro.dibella:b0b9d0c2-8358-4de7-b754-23e63a4505d7 dbms:postgresql
CREATE TABLE IF NOT EXISTS data.student
(
  person_id uuid NOT NULL,
  registration_ts timestamp with time zone NOT NULL DEFAULT current_timestamp,

  digest text GENERATED ALWAYS AS ( to_digest(
        to_text(refs),
        to_text(person_id),        
        to_text(registration_ts)
  ) ) STORED,

  PRIMARY KEY (gid),
  CONSTRAINT fk_person_id FOREIGN KEY(person_id) REFERENCES data.person(gid) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED

) INHERITS (common.identifiable, common.auditable, common.soft_deletable, common.referenceable, common.versioned);

CREATE UNIQUE INDEX student_id_idx ON data.student USING BTREE (id);
CREATE INDEX student_refs_idx ON data.student USING GIN (refs);
CREATE INDEX student_person_id_idx ON data.student USING HASH (person_id);

CREATE SEQUENCE data.student_id_seq START 1 NO CYCLE OWNED BY data.student.id;

ALTER TABLE data.student ALTER COLUMN id SET DEFAULT nextval('data.student_id_seq');

ALTER TABLE data.student OWNER TO ${database.admin.role};
GRANT SELECT, UPDATE, INSERT ON data.student TO ${database.user.role};

ALTER SEQUENCE data.student_id_seq OWNER TO ${database.admin.role};
GRANT USAGE ON data.student_id_seq TO ${database.user.role};


