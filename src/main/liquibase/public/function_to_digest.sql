--liquibase formatted sql

--changeset alessandro.dibella:8088fa6b-5633-4a5a-9dd2-628ee7b80771 dbms:postgresql


CREATE OR REPLACE FUNCTION to_digest(variadic anyarray) returns text AS $$
      SELECT encode(digest(array_to_string($1,''), 'sha1'), 'hex')
$$ LANGUAGE SQL STRICT IMMUTABLE;

ALTER FUNCTION to_digest(anyarray) OWNER TO ${database.admin.role};
GRANT EXECUTE ON FUNCTION to_digest(anyarray) TO ${database.user.role};
