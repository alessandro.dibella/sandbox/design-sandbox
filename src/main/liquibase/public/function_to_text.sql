--liquibase formatted sql

--changeset alessandro.dibella:608f47f3-7270-484c-bc22-d3b24327f17b dbms:postgresql

-- Anything fuction that depends on the code page or collation sequence could potentially change in flight if the locale settings change.
-- Because nearly every text function is locale dependent, it's not possible to use them in generated colums.
-- See https://www.2ndquadrant.com/en/blog/generated-columns-in-postgresql-12/


CREATE OR REPLACE FUNCTION to_text(anyelement) returns text AS $$
      SELECT CAST($1 AS text)
$$ LANGUAGE SQL STRICT IMMUTABLE;


ALTER FUNCTION to_text(anyelement) OWNER TO ${database.admin.role};
GRANT EXECUTE ON FUNCTION to_text(anyelement) TO ${database.user.role};