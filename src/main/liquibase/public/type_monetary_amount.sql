--liquibase formatted sql

--changeset alessandro.dibella:c91d6a13-b7ba-4063-82af-c4b7e12af010 dbms:postgresql

CREATE TYPE monetary_amount AS (
    currency character(3), 
    amount numeric(24,6)
);

CREATE FUNCTION text_to_monetary_amount(text) RETURNS monetary_amount AS $$
    SELECT value[1], value[2]::numeric(24,6) FROM regexp_matches($1, '^([a-zA-Z]{3})[\s]+([\d\.]+)$', 'i') AS value
$$ LANGUAGE SQL STRICT IMMUTABLE;


CREATE FUNCTION monetary_amount_to_text(monetary_amount) RETURNS text AS $$
    SELECT $1.currency || ' ' || $1.amount
$$ LANGUAGE SQL STRICT IMMUTABLE;


CREATE CAST (monetary_amount AS text) WITH FUNCTION monetary_amount_to_text(monetary_amount) AS IMPLICIT;

CREATE CAST (text AS monetary_amount) WITH FUNCTION text_to_monetary_amount(text) AS IMPLICIT;


ALTER TYPE monetary_amount OWNER TO ${database.admin.role};
GRANT USAGE ON TYPE monetary_amount TO ${database.user.role};

ALTER FUNCTION text_to_monetary_amount(text) OWNER TO ${database.admin.role};
GRANT EXECUTE ON FUNCTION text_to_monetary_amount(text) TO ${database.user.role};

ALTER FUNCTION monetary_amount_to_text(monetary_amount) OWNER TO ${database.admin.role};
GRANT EXECUTE ON FUNCTION monetary_amount_to_text(monetary_amount) TO ${database.user.role};