--liquibase formatted sql

--changeset alessandro.dibella:de150b9b-d7e3-44e8-abe4-64af05b63ab0 dbms:postgresql
CREATE TABLE IF NOT EXISTS common.referenceable
(
    -- id bigserial NOT NULL, -- If we use bigserial here, there will be a single sequence for all sub-tables
    id bigint NOT NULL,
    refs jsonb
);

ALTER TABLE common.referenceable OWNER TO ${database.admin.role};

GRANT SELECT, UPDATE, INSERT ON common.referenceable TO ${database.user.role};