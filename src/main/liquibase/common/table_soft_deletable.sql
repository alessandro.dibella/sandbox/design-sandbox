--liquibase formatted sql

--changeset alessandro.dibella:556d804e-1c0e-4dbd-86e4-f41f466b7407 dbms:postgresql
CREATE TABLE IF NOT EXISTS common.soft_deletable
(
  deletion_ts timestamp with time zone
);

ALTER TABLE common.soft_deletable OWNER TO ${database.admin.role};

GRANT SELECT, UPDATE, INSERT ON common.soft_deletable TO ${database.user.role};