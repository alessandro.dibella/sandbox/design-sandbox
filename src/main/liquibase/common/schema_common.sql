--liquibase formatted sql

--changeset alessandro.dibella:1a438907-fce9-4f70-a334-8b6b53d0eb87 dbms:postgresql
CREATE SCHEMA  IF NOT EXISTS common AUTHORIZATION ${database.admin.role};


--changeset alessandro.dibella:a770af98-a436-44a5-966a-2f1c9081f355 dbms:postgresql
GRANT USAGE ON SCHEMA common TO ${database.user.role};