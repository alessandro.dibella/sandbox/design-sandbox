--liquibase formatted sql

--changeset alessandro.dibella:76b15f6f-4396-44ef-8074-43580d9a5825 dbms:postgresql
CREATE TABLE IF NOT EXISTS common.versioned
(
    digest text NOT NULL
);

ALTER TABLE common.versioned OWNER TO ${database.admin.role};

GRANT SELECT, UPDATE, INSERT ON common.versioned TO ${database.user.role};