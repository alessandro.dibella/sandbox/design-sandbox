--liquibase formatted sql

--changeset alessandro.dibella:bce7e279-3ed2-44fa-b26e-cf0cee58f18f dbms:postgresql
CREATE TABLE IF NOT EXISTS common.auditable
(
  last_update_tx bigint NOT NULL DEFAULT txid_current(),
  creation_ts timestamp with time zone NOT NULL DEFAULT current_timestamp,
  last_update_ts timestamp with time zone NOT NULL DEFAULT current_timestamp  
);

ALTER TABLE common.auditable OWNER TO ${database.admin.role};

GRANT SELECT, UPDATE, INSERT ON common.auditable TO ${database.user.role};