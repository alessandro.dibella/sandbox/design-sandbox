--liquibase formatted sql

--changeset alessandro.dibella:d0c4761a-1436-4917-8876-8a2d4059e233 dbms:postgresql
CREATE TABLE IF NOT EXISTS common.identifiable
(
    gid uuid NOT NULL DEFAULT uuid_generate_v4()
);

ALTER TABLE common.identifiable OWNER TO ${database.admin.role};

GRANT SELECT, UPDATE, INSERT ON common.identifiable TO ${database.user.role};