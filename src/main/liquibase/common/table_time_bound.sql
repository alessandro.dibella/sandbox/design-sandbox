--liquibase formatted sql

--changeset alessandro.dibella:389d169d-713f-45ac-9602-e03b63c401d1 dbms:postgresql
CREATE TABLE IF NOT EXISTS common.time_bound
(
  activation_ts timestamp with time zone NOT NULL DEFAULT current_timestamp,
  expriration_ts timestamp with time zone
);

ALTER TABLE common.time_bound OWNER TO ${database.admin.role};

GRANT SELECT, UPDATE, INSERT ON common.time_bound TO ${database.user.role};