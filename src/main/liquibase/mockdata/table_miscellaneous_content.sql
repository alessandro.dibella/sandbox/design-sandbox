--liquibase formatted sql

--changeset alessandro.dibella:520b7fd7c-df12-4bab-bd76-cbdfb331349a dbms:postgresql
CREATE TABLE IF NOT EXISTS mockdata.miscellaneous_content
(
  string text,
  integer_number bigint,
  decimal_number double precision,
  json_obj jsonb,
  monetary_value numeric(10,6),
  string_array text[],
  decimal_array double precision[],
  ts_array timestamp with time zone[],

  PRIMARY KEY (gid)

) INHERITS (common.identifiable, common.auditable, common.soft_deletable, common.referenceable);

CREATE SEQUENCE mockdata.miscellaneous_content_seq START 1 OWNED BY mockdata.miscellaneous_content.id NO CYCLE;

ALTER TABLE mockdata.miscellaneous_content OWNER TO ${database.admin.role};

GRANT SELECT, UPDATE, INSERT ON mockdata.miscellaneous_content TO ${database.user.role};

ALTER SEQUENCE mockdata.miscellaneous_content_seq OWNER TO ${database.admin.role};

GRANT USAGE ON mockdata.miscellaneous_content_seq TO ${database.user.role};

CREATE INDEX miscellaneous_content_id_idx ON mockdata.miscellaneous_content USING btree (id);