--liquibase formatted sql

--changeset alessandro.dibella:e9a0a66a-4d63-4c63-a6b0-5b8792bf6b26 dbms:postgresql
CREATE SCHEMA  IF NOT EXISTS mockdata AUTHORIZATION ${database.admin.role};


--changeset alessandro.dibella:1dd1309a-aa22-41d3-8839-f70ec84bbf22 dbms:postgresql
GRANT USAGE ON SCHEMA mockdata TO ${database.user.role};