CREATE ROLE :database_user_role WITH NOLOGIN INHERIT BYPASSRLS;
CREATE ROLE :database_user_username LOGIN INHERIT IN ROLE :database_user_role;
ALTER ROLE :database_user_username WITH PASSWORD :'database_user_password';

CREATE ROLE :database_admin_role WITH NOLOGIN INHERIT BYPASSRLS;
CREATE ROLE :database_admin_username LOGIN INHERIT IN ROLE :database_admin_role;
ALTER ROLE :database_admin_username WITH PASSWORD :'database_admin_password';

GRANT :database_user_role to :database_admin_role;

CREATE DATABASE :database_name WITH OWNER=:database_admin_role ENCODING='UTF8';


